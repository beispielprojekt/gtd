package control;

import abstractuserinterfaces.ContextlistAUI;
import abstractuserinterfaces.InboxAUI;
import abstractuserinterfaces.JobAUI;
import abstractuserinterfaces.ProjectAUI;

/**
 * Hilfsklasse zum Erzeugen der verschiedenen DummyAUIs zum Testen.
 * @author Christian Riest
 *
 */
public class AUIGenerator {

	public static JobAUI jobsAUI(){


		JobAUI jobsAUI = new JobAUI() {
			
			@Override
			public void refreshJobs() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void refreshJob() {
				// TODO Auto-generated method stub
				
			}
		};
		return jobsAUI;
	}
	
	public static ContextlistAUI contextlistsAUI(){


		ContextlistAUI contextlistsAUI = new ContextlistAUI() {
			
			@Override
			public void refreshContextlists() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void refreshContextlist() {
				// TODO Auto-generated method stub
				
			}

		
			
		};
		return contextlistsAUI;
	}
	
	public static InboxAUI inboxAUI() {
		InboxAUI inboxAUI = new InboxAUI() {
	
			
			@Override
			public void refreshInbox() {
				// TODO Auto-generated method stub
				
			}
		};
		return inboxAUI;
	}
	
	public static ProjectAUI projectAUI() {
		ProjectAUI projectTreeAUI = new ProjectAUI() {
			
			@Override
			public void refreshProjectTree() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void refreshProject() {
				// TODO Auto-generated method stub
				
			}
		};
		return projectTreeAUI;
	}
	
	
	
}
