package controltestsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import control.GtdControllerConstructorTest;
import control.GtdControllerSetGtdTest;

/**
 * TestSuite zum Starten aller GtdController-Tests.
 *
 * @author Florian
 */
@RunWith(Suite.class)
@SuiteClasses({ GtdControllerConstructorTest.class, GtdControllerSetGtdTest.class, })
public class TestSuiteGtdController {

}