/**
 * Dieses Package enthält TestSuites mit denen sich alle Tests für eine jeweilge Klasse
 * sowie für die Varianten A und B des Aufwärmprojektes gesammelt ausführen lassen.
 */
package controltestsuites;