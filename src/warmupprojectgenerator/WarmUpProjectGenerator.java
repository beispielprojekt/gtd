package warmupprojectgenerator;

import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;

public class WarmUpProjectGenerator {// NOPMD
	private static final String VARIANTS = "AB";
	private static final String SEARCH_ALL1 = "(?ms)\t\t// EXCLUDE<ALL.*?// EXCLUDE>ALL\n";
	private static final String SEARCH_ALL2 = "(?ms)\t\t// EXCLUDE<ALL:.*?// EXCLUDE>ALL:\\s?";
	private static final String SEARCH_VARIANT1 = "(?ms)\t\t// EXCLUDE<%s.*?// EXCLUDE>%s\n";
	private static final String SEARCH_VARIANT2 = "(?ms)\t\t// EXCLUDE<%s:.*?// EXCLUDE>%s:\\s?";
	private static final String SEARCH_REST = "\t\t// EXCLUDE[<>][%s].*?\n";
	private static final String REPLACE_ALL1 = "\t\t// Diese Funktion muss im Aufwärmprojekt nicht implementiert werden.\n";
	private static final String REPLACE_ALL2 = REPLACE_ALL1 + "\t\t";
	private static final String REPLACE_VARIANT1 = "\t\t// TODO Implementiere diese Methode, um die Spezifikationen von Ihrem Handout zu erfüllen.\n";
	private static final String REPLACE_VARIANT2 = REPLACE_VARIANT1 + "\t\t";
	private static ArrayList<File> excludeFiles;
	private static String variantText;
	private static LinkedHashMap<String, String> replacements;

	public static void main(String[] args) {
		File folderProjectRoot = new File("").getAbsoluteFile();
		File folderExport = new File(folderProjectRoot, "build");
		excludeFiles = new ArrayList<File>();
		excludeFiles.add(new File(folderProjectRoot, "bin"));
		excludeFiles.add(new File(folderProjectRoot, "src" + File.separator + "view"));
		excludeFiles.add(new File(folderProjectRoot,
				"src" + File.separator + WarmUpProjectGenerator.class.getPackage().getName()));
		excludeFiles.add(folderExport);
		folderExport.mkdirs();

		for (String variant : VARIANTS.split("")) {
			variantText = "Variante_" + variant;
			replacements = new LinkedHashMap<>();
			replacements.put(SEARCH_ALL2, REPLACE_ALL2);
			replacements.put(SEARCH_ALL1, REPLACE_ALL1);
			replacements.put(String.format(SEARCH_VARIANT2, variant, variant), REPLACE_VARIANT2);
			replacements.put(String.format(SEARCH_VARIANT1, variant, variant), REPLACE_VARIANT1);
			replacements.put(String.format(SEARCH_REST, VARIANTS, VARIANTS), "");

			File folderVariant = new File(folderExport, variantText);
			File folderTarget = new File(folderVariant, folderProjectRoot.getName() + "_" + variantText);
			if (folderTarget.exists()) {
				int ret = JOptionPane.showConfirmDialog(null,
						String.format("Ziel-Exportverzeichnis '%s' existiert bereits. Ersetzen?", variant), "Ersetzen?",
						JOptionPane.YES_NO_OPTION);
				if (ret == JOptionPane.NO_OPTION) {
					System.out.println("Export wird abbgebrochen!");
					return;
				}
				rekDelete(folderTarget);
				folderTarget.getParentFile().delete();
			}
			try {
				copyRek(folderProjectRoot, folderTarget, variant);
				zipDir(folderTarget, new File(folderVariant, variantText.toLowerCase() + ".zip"));
				System.out.println("Projekt '" + variantText + "' exportiert.");
			} catch (IOException exception) {
				System.err.println("Error: " + exception.getMessage());
			}
		}
		try {
			Desktop.getDesktop().open(folderExport);
		} catch (IOException e) {
		}
	}

	private static void copyRek(File src, File dst, String variant) throws IOException {// NOPMD
		if (excludeFiles.contains(src) || src.getName().equals(".svn")) {
			return;
		}
		if (src.isDirectory()) {
			if (dst.exists()) {
				throw new IllegalStateException("Zielverzeichnis existiert bereits");
			}
			dst.mkdirs();
			File[] srcSubfiles = src.listFiles();
			if (srcSubfiles != null) {
				for (File srcSubfile : srcSubfiles) {
					copyRek(srcSubfile, new File(dst, srcSubfile.getName()), variant);
				}
			}
		} else {
			if (src.getName().endsWith(".java")) {
				String text = String.join("\n", Files.readAllLines(src.toPath(), StandardCharsets.UTF_8));
				for (Entry<String, String> entrySet : replacements.entrySet()) {
					text = text.replaceAll(entrySet.getKey(), entrySet.getValue());
				}
				Files.write(dst.toPath(), Arrays.asList(text.trim()), StandardCharsets.UTF_8);
			} else if (src.getName().equals(".project")) {
				String text = String.join("\n", Files.readAllLines(src.toPath(), StandardCharsets.UTF_8));
				text = text.replace("<name>Beispielprojekt_GTD_JavaFX</name>",
						"<name>Beispielprojekt_GTD_JavaFX_Variante_" + variant + "</name>");
				Files.write(dst.toPath(), Arrays.asList(text.trim()), StandardCharsets.UTF_8);
			} else {
				cloneFile(src, dst);
			}
		}
	}

	private static void cloneFile(File src, File dst) throws IOException {
		BufferedInputStream bufferedInputStream = null;
		BufferedOutputStream bufferedOutputStream = null;
		byte[] buffer = new byte[1024];
		int bytes = 0;
		try {
			bufferedInputStream = new BufferedInputStream(new FileInputStream(src));
			bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(dst));
			// Kopiere byte fuer byte
			while ((bytes = bufferedInputStream.read(buffer)) != -1) {
				bufferedOutputStream.write(buffer, 0, bytes);
			}
		} catch (IOException ioException) {
			if (bufferedInputStream != null) {
				bufferedInputStream.close();
			}
			if (bufferedOutputStream != null) {
				bufferedOutputStream.close();
			}
			throw ioException;
		} finally {
			if (bufferedInputStream != null) {
				bufferedInputStream.close();
			}
			if (bufferedOutputStream != null) {
				bufferedOutputStream.close();
			}
		}
	}

	private static void rekDelete(File file) {
		if (file.isDirectory()) {
			File[] subfiles = file.listFiles();
			if (subfiles != null) {
				for (File subfile : subfiles) {
					rekDelete(subfile);
				}
			}
		}
		file.delete();
	}

	private static void zipDir(File dir, File zipFile) throws IOException {
		FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
		ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
		addFolderToZip("", dir.getAbsolutePath(), zipOutputStream);
		zipOutputStream.close();
		fileOutputStream.close();
	}

	private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zipOutputStream)
			throws IOException {
		File folder = new File(srcFolder);
		if (folder.list().length == 0) {
			addFileToZip(path, srcFolder, zipOutputStream, true);
		} else {
			for (String fileName : folder.list()) {
				if (path.equals("")) {
					addFileToZip(folder.getName(), srcFolder + "/" + fileName, zipOutputStream, false);
				} else {
					addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zipOutputStream, false);
				}
			}
		}
	}

	private static void addFileToZip(String path, String srcFile, ZipOutputStream zipOutputStream, boolean flag)
			throws IOException {
		File folder = new File(srcFile);
		if (flag) {
			zipOutputStream.putNextEntry(new ZipEntry(path + "/" + folder.getName() + "/"));
		} else {
			if (folder.isDirectory()) {
				addFolderToZip(path, srcFile, zipOutputStream);
			} else {
				byte[] buf = new byte[1024];
				int len;
				FileInputStream fileInputStream = new FileInputStream(srcFile);
				zipOutputStream.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
				while ((len = fileInputStream.read(buf)) > 0) {
					zipOutputStream.write(buf, 0, len);
				}
				fileInputStream.close();
			}
		}
	}

}