/**
 * Dieses Package enthält die AbstractUserInterface-Klassen,
 * mittels denen die GUI benachrichtigt werden kann,
 * dass sie sich aktualisieren muss.
 */
package abstractuserinterfaces;