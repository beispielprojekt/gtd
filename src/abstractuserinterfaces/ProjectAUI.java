package abstractuserinterfaces;

/**
 * AUI für die ProjectTree-View.
 * @author Christian Riest
 *
 */
public interface ProjectAUI {

	/**
	 * Beim Aufruf wird as angezeigte Projekt in der Project-View mit den aktuellen Daten neu gezeichnet.
	 */
	public void refreshProject();

	/**
	 * Beim Aufruf wird der ProjectTree in der Project-View mit den aktuellen Daten neu gezeichnet.
	 */
	public void refreshProjectTree();

}