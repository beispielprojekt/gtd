package abstractuserinterfaces;


/**
 * AUI für die Inbox-View.
 * @author Christian 
 *
 */
public interface InboxAUI {

	/**
	 * Aktualisiert die Inbox-View.
	 */
	public void refreshInbox();


}