package abstractuserinterfaces;

/**
 * AUI für die Contextlists-View.
 * @author Christian Riest
 *
 */
public interface ContextlistAUI { 
	
	
	/**
	 * Beim Aufruf wird die Liste an Jobs sowie der angezeigte Titel in derContextlists-View mittels der aktuellen Daten neu gezeichnet.
	 */
	public void refreshContextlist();
	
	/**
	 * Beim Aufruf wird die Liste der Contextlists in der Contextlists-View mittels der aktuellen Daten neu gezeichnet.
	 */
	public void refreshContextlists();
	
	


}