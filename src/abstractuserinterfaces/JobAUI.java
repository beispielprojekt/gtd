package abstractuserinterfaces;

/**
 * AUI für die Jobs-View.
 * @author Christian Riest
 *
 */
public interface JobAUI {

	/**
	 * Refreshed den aktuell angezeigten Job in der Job-View mit den aktuellen Daten.
	 */
	public void refreshJob();

	/**
	 * Refreshed die Tabelle aller Jobs in der Jobs-View mit den aktuellen Daten.
	 */
	public void refreshJobs();

}