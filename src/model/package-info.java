/**
 * Dieses Package enthält alle Klassen, die der Model-Schicht zugeordnet sind.
 * Sie werden verwendet, um Daten zu speichern und zu verwalten.
 */
package model;