package model;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Die Inbox enthält eine Liste von Notizen und den Zeitpunkt der letzten
 * Notizkonvertierung.
 *
	@author Irgendjemand anders heute

 */
public class Inbox {
	/**
	 * Liste aller Notizen.
	 */
	private ArrayList<Note> notes;
	/**
	 * Zeitpunkt der letzten Notizkonvertierung.
	 */
	private LocalDateTime lastNoteConversion;

	/**
	 * Konstruktor, erzeugt neues Inbox-Objekt. Die Liste der Notizen und der
	 * Zeitpunkt der letzten Notizkonvertierung werden initialisiert.
	 */
	public Inbox() {
		notes = new ArrayList<Note>();
		updateLastNoteConversion();
	}

	/**
	 * Hängt eine Notiz an das Ende der Notiz-Liste.
	 *
	 * @param note
	 *            Die neue Notiz.
	 */
	public void addNote(Note note) {
		notes.add(note);
	}

	/**
	 * Gibt den Zeitpunkt der letzten Notizkonvertierung zurück.
	 *
	 * @return Zeitpunkt der letzten Notizkonvertierung.
	 */
	public LocalDateTime getLastNoteConversion() {
		return lastNoteConversion;
	}

	/**
	 * Gibt die Liste aller Notizen zurück.
	 *
	 * @return Liste aller Notizen.
	 */
	public ArrayList<Note> getList() {
		return notes;
	}

	/**
	 * Entfernt eine Notiz, falls vorhanden, aus der Liste der Notizen.
	 *
	 * @param note
	 *            Die zu entfernende Notiz.
	 * @return <strong>true</strong> Notiz war vorhanden und wurde entfernt,<br>
	 *         <strong>false</strong> Notiz nicht vorhanden.
	 */
	public boolean removeNote(Note note) {
		return notes.remove(note);
	}

	/**
	 * Setzt das Datum der letzten Notizkonvertierung auf den aktuellen
	 * Zeitpunkt.
	 */
	public void updateLastNoteConversion() {
		this.lastNoteConversion = LocalDateTime.now();
	}

}