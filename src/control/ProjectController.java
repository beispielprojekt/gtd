package control;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import abstractuserinterfaces.ProjectAUI;
import exceptions.EmptyStringParameterException;
import exceptions.GtdProjectException;
import exceptions.ObjectNotInGtdException;
import model.Job;
import model.Project;
import model.TreeNode;

/**
 * Controller für Funktionen zur Verwaltung der Projekte.
 *
 * @author Florian
 */
public class ProjectController {
	private static final int TREEPATH_MINSIZE_CONTAINS_TREENODE = 1;
	/**
	 * Die Referenz auf den zentralen Controller, der zum Austausch zwischen den
	 * Controllern dient.
	 */
	private GtdController gtdController;
	/**
	 * Referenz auf die{@link ProjectAUI}.
	 */
	private ProjectAUI projectAUI;

	/**
	 * Konstruktor.
	 *
	 * @param gtdController
	 *            Die Referenz auf den zentralen Controller, der zum Austausch
	 *            zwischen den Controllern dient.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der Parameter
	 *             <em>null</em> ist.
	 * @postconditions Das Attribut gtdController verweist auf den zentralen
	 *                 Controller, sofern nicht <em>null</em> übergeben wurde.
	 */
	protected ProjectController(GtdController gtdController) throws NullPointerException {
		ParamVal.objNotNull(gtdController, "gtdController");
		this.gtdController = gtdController;
	}





	/**
	 * Erzeugt ein neues Projekt mit einem eindeutigen Titel.<br>
	 * Der Titel des Projekts muss im Projektbaum eindeutig sein.
	 * 
	 * @param parent
	 *            Ein Vaterprojekt oder die Wurzel des Projektbaums.
	 * @param title
	 *            Der Titel des (neuen) Projekts. Der Titel des Projekts darf
	 *            nicht <em>null</em> und nicht leer sein und muss eindeutig
	 *            innerhalb des Projektbaums sein.
	 * @param completeUntil
	 *            Das Datum, bis wann das Projekt erledigt sein soll.
	 * @param description
	 *            Die detaillierte Beschreibung des Projekts. Diese darf nicht
	 *            <em>null</em>, aber leer sein.
	 * @return Das erstellte Projekt.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen Parameter <em>null</em> ist.
	 * @throws EmptyStringParameterException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen String-Parameter (bis auf die Beschreibung) leer
	 *             ist.
	 * @throws IllegalArgumentException
	 *             Die Runtime-Exception wird geworfen, wenn das übergebene
	 *             Datum in der Vergangenheit liegt.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             TreeNode parent nicht in Gtd enthalten ist.
	 * @throws GtdProjectException
	 *             Die Exception wird geworfen, wenn ein Projekt mit dieser
	 *             Bezeichnung bereits existiert.
	 */
	public Project createProject(TreeNode parent, String title, LocalDate completeUntil, String description)
			throws NullPointerException, EmptyStringParameterException, IllegalArgumentException,
			ObjectNotInGtdException, GtdProjectException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(parent, "parent");
		ParamVal.stringNotNullOrEmpty(title, "title");
		ParamVal.dateNotNullAndInFuture(completeUntil, "completeUntil");
		ParamVal.objNotNull(description, "description");
		// Überprüfe ob parent in Gtd
		if (!parent.equals(gtdController.getGtd().getRootNode())) {
			if (parent instanceof Project) {
				TreeNode parentTreeNode = searchParentTreeNode((Project) parent);
				if (parentTreeNode == null) {
					throw new ObjectNotInGtdException("Das Project(TreeNode) parent ist nicht in Gtd enthalten.");
				}
			} else {
				throw new ObjectNotInGtdException("Der TreeNode parent ist nicht in Gtd enthalten.");
			}
		}
		if (existProjectTitle(title)) {
			throw new GtdProjectException("Ein Projekt mit Bezeichnung '" + title + "' existiert bereits.");
		}
		// Kein Projekt mit gleichem Titel, neues Projekt anlegen.
		Project project = new Project(title, completeUntil, description);
		parent.addSubproject(project);
		// aktualisiere Stati der Projekte
		updateFinishedStatusInAllProjects();
		this.projectAUI.refreshProjectTree();
		return project;
		
	}

	/**
	 * Überprüft, ob ein Projekt im Projektbaum mit dem Titel bereits existiert.
	 *
	 * @param title
	 *            Der Titel für die Suche.
	 * @return true, wenn bereits ein Projekt mit Titel existiert, sonst false
	 * @preconditions title kein leerer String.
	 */
	protected boolean existProjectTitle(String title) {
		return existProjectTitleRek(title, gtdController.getGtd().getRootNode());
	}

	/**
	 * Rekursive Subfunktion.
	 */
	private boolean existProjectTitleRek(String title, TreeNode treeNode) {
		
		for (Project project : treeNode.getSubprojects()) {
			if (project.getTitle().equals(title) || existProjectTitleRek(title, project)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Erzeugt eine Liste mit allen fertigen Projekten der Projektbaumwurzel.
	 * <br>
	 * Wenn keine Projekte im Projektbaum sind oder alle nicht fertig sind, wird
	 * eine leere Liste zurückgegeben.
	 *
	 * @return Liste mit allen fertigen Projekten oder leere Liste
	 */
	protected ArrayList<Project> getFinishedRootProjects() {
		
		ArrayList<Project> list = new ArrayList<>();
		ArrayList<Project> subprojects = gtdController.getGtd().getRootNode().getSubprojects();
		for (Project project : subprojects) {
			if (project.isFinished()) {
				list.add(project);
			}
		}
		return list;
		
	}

	/**
	 * Berechnet den Pfad von der Projektbaumwurzel bis zum Projektbaumknoten.
	 * <br>
	 * Wenn der Endknoten (treeNode) nicht im Projektbaum ist, wird eine leere
	 * Liste zurückgegeben.
	 *
	 * @param treeNode
	 *            Der Endknoten des Pfades.
	 * @return Pfad von Projektbaumwurzel bis Projektbaumknoten oder leere
	 *         Liste.
	 */
	public ArrayList<TreeNode> getTreeNodePath(TreeNode treeNode) {
		
		ParamVal.objNotNull(treeNode, "treeNode");
		ArrayList<TreeNode> projectTreePath = new ArrayList<>();
		getTreeNodePathRek(projectTreePath, treeNode, gtdController.getGtd().getRootNode());
		return projectTreePath;
		
	}

	/**
	 * Rekursive Subfunktion zu Bestimmung des TreeNodePath.
	 *
	 * @param projectTreePath
	 *            Liste mit aktuellem TreeNodePath
	 * @param treeNode
	 *            Der Endknoten des Pfades.
	 * @param currentTreeNode
	 *            Der aktuelle Projektbaumknoten
	 * @return true wenn der Endknoten erreicht wurde, ansonsten false.
	 */
	private boolean getTreeNodePathRek(ArrayList<TreeNode> projectTreePath, TreeNode treeNode,
			TreeNode currentTreeNode) {
		
		projectTreePath.add(currentTreeNode);
		if (treeNode.equals(currentTreeNode)) {
			return true;
		} else {
			for (TreeNode subTreeNode : currentTreeNode.getSubprojects()) {
				if (getTreeNodePathRek(projectTreePath, treeNode, subTreeNode)) {
					return true;
				}
			}
			projectTreePath.remove(currentTreeNode);
			return false;
		}

	}

	/**
	 * Verschiebt ein Projekt in ein anderes Projekt oder die Projektbaumwurzel.
	 *
	 * @param project
	 *            Das zu verschiebene Projekt.
	 * @param newParent
	 *            Der neue Vaterknoten des Projekts (ein Projekt oder die
	 *            Projektbaumwurzel).
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der Parameter
	 *             <em>null</em> ist.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn einer der Parameter
	 *             nicht in Gtd enthalten ist.
	 * @throws GtdProjectException
	 *             Die Exception wird geworfen, wenn das Projekt nicht
	 *             verschoben werden kann.
	 */
	public void moveProject(Project project, TreeNode newParent)
			throws NullPointerException, ObjectNotInGtdException, GtdProjectException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(project, "project");
		ParamVal.objNotNull(newParent, "newParent");
		// Überprüfe ob project in Gtd
		TreeNode oldParent = searchParentTreeNode(project);
		if (oldParent == null) {
			throw new ObjectNotInGtdException("Das Projekt ist nicht in Gtd enthalten.");
		}
		if (oldParent.equals(newParent)) {
			// keine Änderung
			return;
		}
		// ermittle treePath von newParent
		ArrayList<TreeNode> newParentTreeNodePath = getTreeNodePath(newParent);
		// Überprüfe ob newParent in Gtd
		if (newParentTreeNodePath.size() < TREEPATH_MINSIZE_CONTAINS_TREENODE) {
			throw new ObjectNotInGtdException("Der TreeNode newParent ist nicht in Gtd enthalten.");
		}
		// ist project==newParent
		if (project.equals(newParent)) {
			throw new GtdProjectException("Das Projekt kann nicht in sich selbst verschoben werden.");
		}
		// überprüfe ob project kindknoten von newParent
		if (newParentTreeNodePath.contains(project)
				&& newParentTreeNodePath.indexOf(project) < newParentTreeNodePath.indexOf(newParent)) {
			// newParent ist im Projektunterbaum von project.
			throw new GtdProjectException(
					"Das Projekt kann nicht in ein eigenes (nte-)Unterprojekt verschoben werden.");
		}
		oldParent.removeSubproject(project);
		newParent.addSubproject(project);
		updateFinishedStatusInAllProjects();
		this.projectAUI.refreshProjectTree();
		
	}

	/**
	 * Entfernt ein Projekt aus dem Projektbaum, falls es vorhanden und erledigt
	 * oder ohne Tätigkeiten und Unterprojekte ist.
	 *
	 * @param project
	 *            Das zu entfernende Projekt.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn das übergebene
	 *             Projekt <em>null</em> ist.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn das übergebene
	 *             Projekt nicht in Gtd enthalten ist.
	 * @throws GtdProjectException
	 *             Die Exception wird geworfen, wenn das Projekt nicht erledigt
	 *             ist.
	 */
	public void removeProject(Project project)
			throws NullPointerException, ObjectNotInGtdException, GtdProjectException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(project, "project");

		TreeNode parentTreeNode = searchParentTreeNode(project);
		if (parentTreeNode == null) {
			throw new ObjectNotInGtdException("Das Projekt ist nicht in Gtd enthalten.");
		}
		if (project.isFinished() || (project.getJobs().size() == 0 && project.getSubprojects().size() == 0)) {
			// Projekt ist erledigt, entferne Projekt.
			parentTreeNode.removeSubproject(project);
			if (parentTreeNode instanceof Project) {
				((Project) parentTreeNode).updateFinishedStatus();
			}
			this.projectAUI.refreshProjectTree();
		} else {
			// Projekt ist nicht erledigt und enthält Tätigkeiten.
			throw new GtdProjectException("Das Projekt '" + project.getTitle()
					+ "' kann nicht entfernt werden, weil es noch nicht erledigt ist.");
		}
		
	}

	/**
	 * Sucht den Vaterknoten für ein Projekt(-Kindknoten).<br>
	 * Wenn das Projekt keine Vater hat (also nicht im Projektbaum ist), wird
	 * <em>null</em> zurückgegeben.
	 *
	 * @param project
	 *            Das Projekt für die Suche. Darf nicht <em>null</em> sein.
	 * @return Den Vaterknoten (TreeNode) oder <em>null</em>.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der Parameter
	 *             <em>null</em> ist.
	 */
	public TreeNode searchParentTreeNode(Project project) throws NullPointerException {
		
		ParamVal.objNotNull(project, "project");
		// wenn 1, wäre es der RootNode (parent(rootNode)=null)
		// wenn 0, ist das Project nicht im Projektbaum
		ArrayList<TreeNode> treeNodePath = getTreeNodePath(project);
		if (treeNodePath.size() > TREEPATH_MINSIZE_CONTAINS_TREENODE) {
			return treeNodePath.get(treeNodePath.indexOf(project) - 1);
		}
		
		return null;
	}

	/**
	 * Aktualisiert den Erledgt-Status in allen Projekten.
	 */
	protected void updateFinishedStatusInAllProjects() {
		ArrayList<Project> subprojects = gtdController.getGtd().getRootNode().getSubprojects();
		for (Project project : subprojects) {
			project.updateFinishedStatus();
		}
		this.projectAUI.refreshProject();
	}

	/**
	 * Setzt in allen Projekten den nächsten Schritt auf die erste
	 * nicht-erledigte Tätigkeit des Projekts oder auf <em>null</em> wenn keine
	 * existiert und aktualisiert dann den Projektstatus.
	 */
	protected void updateNextJobInAllProjects() {
		
		ArrayList<Project> subprojects = gtdController.getGtd().getRootNode().getSubprojects();
		for (Project project : subprojects) {
			project.updateNextJob();
		}
		this.projectAUI.refreshProject();
		
	}
	
	
	/**
	 * Berechnet die relative Anzahl an beendeten Jobs in einem Projekt.
	 * @param project Das Projekt, für welches die relavtive Anzahl bestimmt werden soll.
	 * @return Die relative Anzahl.
	 */
	public double calculateProjectProgress(Project project) {
		
		
		List<Job> jobs =  project.getJobs();
		if(jobs.size() == 0)
			return 1.0;
		double finishedJobs = 0;
		
		for(Job job:jobs) {
			if(job.isFinished()) {
				finishedJobs ++;
			}
		}
		return finishedJobs/jobs.size();
	}
	
	
	
	/**
	 * Gibt alle in Projektbaum gespeicherten Projekte als Liste zurück.
	 * @return Liste aller Projekte.
	 */
	public List<TreeNode> allTreeNodes(){
		List<TreeNode> allTreeNodes = new ArrayList<TreeNode>();
		TreeNode root = gtdController.getGtd().getRootNode();
		
		allTreeNodesRec(allTreeNodes, root);
		
		return allTreeNodes;
	}
	
	
	/**
	 * Hilfsmethode zur Ermittlung aller Projekte im Projektbaum mittels Rekursion.
	 * @param allTreeNodes Liste aller bisher ermittelten Projekte.
	 * @param root Die wurzel des aktuell durchlaufenden Unterbaums.
	 * @return Liste aller Projekte.
	 */
	private List<TreeNode> allTreeNodesRec(List<TreeNode> allTreeNodes, TreeNode root){
		allTreeNodes.add(root);
		for(TreeNode node : root.getSubprojects()) {
			allTreeNodesRec(allTreeNodes, node);
		}
		return allTreeNodes;
	}
	
	/**
	 * Setze die {@link ProjectAUI}.
	 *
	 * @param projectTreeAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setProjectTreeAUI(ProjectAUI projectTreeAUI) {
		this.projectAUI = projectTreeAUI;
	}
	
	

}