package control;

import java.util.ArrayList;

import abstractuserinterfaces.ContextlistAUI;
import abstractuserinterfaces.JobAUI;
import abstractuserinterfaces.ProjectAUI;
import exceptions.EmptyStringParameterException;
import exceptions.GtdContextlistException;
import exceptions.ObjectNotInGtdException;
import model.Contextlist;

/**
 * Controller für Funktionen zur Verwaltung der Kontextlisten.
 *
 * @author Florian
 */
public class ContextlistController {
	/**
	 * Die Referenz auf den zentralen Controller, der zum Austausch zwischen den
	 * Controllern dient.
	 */
	private GtdController gtdController;
	/**
	 *Referenz zum {@link ContextlistAUI}.
	 */
	private ContextlistAUI contextlistAUI;
	
	
	/**
	 *Referenz zum {@link JobAUI}.
	 */
	private JobAUI jobAUI; 
	
	/**
	 *Referenz zum {@link ProjectAUI}.
	 */	
	private ProjectAUI projectAUI;

	/**
	 * Konstruktor.
	 *
	 * @param gtdController
	 *            Die Referenz auf den zentralen Controller, der zum Austausch
	 *            zwischen den Controllern dient.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der Parameter
	 *             <em>null</em> ist.
	 * @postconditions Das Attribut gtdController verweist auf den zentralen
	 *                 Controller, sofern nicht <em>null</em> übergeben wurde.
	 */
	public ContextlistController(GtdController gtdController) throws NullPointerException {
		
		ParamVal.objNotNull(gtdController, "gtdController");
		this.gtdController = gtdController;
		
	}


	/**
	 * Erzeugt eine neue Kontextliste mit dem übergebenen Titel.<br>
	 * Der Titel der Kontextliste wird nur dann akzeptiert, wenn er innerhalb
	 * des Systems eindeutig ist. Ein neues Kontextlisten-Objekt wird erzeugt,
	 * an Gtd übergeben und dort eingefügt.
	 *
	 * @param title
	 *            Der Titel der (neuen) Kontextliste. Der Titel der Kontextliste
	 *            darf nicht <em>null</em> und nicht leer sein und muss
	 *            eindeutig sein.
	 * @return Die erstellte Kontextliste.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             Titel der Kontextliste <em>null</em> ist.
	 * @throws EmptyStringParameterException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             Titel der Kontextliste leer ist.
	 * @throws GtdContextlistException
	 *             Die Exception wird geworfen, wenn eine Kontextliste mit
	 *             gleichem Bezeichner bereits existiert.
	 */
	public Contextlist createContextlist(String title)
			throws NullPointerException, EmptyStringParameterException, GtdContextlistException {
		
		// Überprüfe Parameter: not null or empty
		ParamVal.stringNotNullOrEmpty(title, "title");

		// Prüfe, ob Kontextliste mit gleichem Titel existiert.
		if (existContextlistTitle(title)) {
			// Kontextliste exisiert bereis mit gleichem Titel.
			throw new GtdContextlistException("Eine Kontextliste mit Titel '" + title + "' existiert bereits.");
		}
		// Kontextliste exisitert nicht, erzeuge neue Kontextliste.
		Contextlist newContextlist = new Contextlist(title);
		gtdController.getGtd().addContextlist(newContextlist);
		this.contextlistAUI.refreshContextlists();
	
		return newContextlist;
		
	}

	/**
	 * Überprüft ob eine Kontextliste mit dem übergebenen Titel exisitert.
	 *
	 * @param title
	 *            Der Titel der Kontextliste.
	 * @return true wenn existiert.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             Titel der Kontextliste <em>null</em> ist.
	 * @throws EmptyStringParameterException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             Titel der Kontextliste leer ist.
	 * @preconditions title kein leerer String.
	 */
	public boolean existContextlistTitle(String title) throws NullPointerException, EmptyStringParameterException {
		
		// Überprüfe Parameter: not null or empty
		ParamVal.stringNotNullOrEmpty(title, "title");

		// Prüfe, ob Kontextliste mit gleichem Titel existiert.
		ArrayList<Contextlist> contextlists = gtdController.getGtd().getContextlists();
	
		for (Contextlist contextlist : contextlists) {
			if (contextlist.getTitle().equals(title)) {
				// Kontextliste gefunden.
				return true;
			}
		}
		// Kontextliste nicht gefunden.
		
		return false;
	}

	/**
	 * Entfernt eine Kontextliste nur dann, wenn sie leer ist oder geleert
	 * werden kann. Bevor überprüft wird, ob die Kontextliste leer ist, werden
	 * alle erledigten Tätigkeiten aus der Kontextliste entfernt.
	 *
	 * @param contextlist
	 *            Die zu entferndende Kontextliste.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn die übergebene
	 *             Kontextliste <em>null</em> ist.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn die übergebene
	 *             Kontextliste nicht in Gtd ist.
	 * @throws GtdContextlistException
	 *             Die Exception wird geworfen, wenn die Kontextliste nicht
	 *             entfernt werden kann, weil die Kontextliste Tätigkeiten
	 *             enthält.
	 */
	public void removeContextlist(Contextlist contextlist)
			throws NullPointerException, ObjectNotInGtdException, GtdContextlistException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(contextlist, "contextlist");

		ArrayList<Contextlist> contextlists = gtdController.getGtd().getContextlists();
		// Ist Kontextliste enthalten.
		if (!contextlists.contains(contextlist)) {
			throw new ObjectNotInGtdException("Die Kontextliste ist nicht in Gtd enthalten.");
		}
		// Sind alle Tätigkeiten erledigt?
		int counterFinishedJobs = 0;
		for (int i = 0; i < contextlist.getJobs().size(); i++) {
			if (contextlist.getJobs().get(i).isFinished()) {
				counterFinishedJobs++;
			}
		}
		if (counterFinishedJobs < contextlist.getJobs().size()) {
			throw new GtdContextlistException(
					"Die Kontextliste enthält noch unerledigte Tätigkeiten und kann deswegen nicht gelöscht werden.");
		}
		if (contextlist.isEmpty()) {
			// Kontextliste leer, entferne Kontextliste.
			gtdController.getGtd().removeContextlist(contextlist);
			this.contextlistAUI.refreshContextlists();
		} else {
			// Kontextliste nicht leer, werfe Exception.
			throw new GtdContextlistException(
					"Die Kontextliste ist nicht leer und kann erst nach dem Aufräumen gelöscht werden.");
		}
		
	}

	/**
	 * Entfernt alle erledigten Tätigkeiten aus allen Kontextlisten.<br>
	 * Wird für die Wochendurchsicht benötigt. Die Kontextlisten werden
	 * nacheinander durchlaufen und für jede Kontextliste das Entfernen der
	 * erledigten Tätigkeiten aufgerufen.
	 *
	 * @return Die Anzahl der entferntenten Tätigkeiten.
	 * @postconditions In allen Kontextliste existieren keine erledigten
	 *                 Tätigkeiten mehr.
	 */
	public int removeFinishedJobsInAllContextlists() {
		int finishedJobs = 0;
		
		ArrayList<Contextlist> contextlists = gtdController.getGtd().getContextlists();
		for (Contextlist contextlist : contextlists) {
			finishedJobs += contextlist.removeAllFinishedJobs();
		}
		contextlistAUI.refreshContextlist();
		jobAUI.refreshJobs();
		projectAUI.refreshProject();
	
		
		return finishedJobs;
	}
	
	
	/**
	 * Setzt die {@link ContextlistAUI}.
	 *
	 * @param contextlistsAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setContextlistsAUI(ContextlistAUI contextlistsAUI) {
		this.contextlistAUI = contextlistsAUI;
	}
	
	
	/**
	 * Setzt die {@link JobAUI}.
	 *
	 * @param jobAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setJobAUI(JobAUI jobAUI) {
		this.jobAUI = jobAUI;
	}
	
	/**
	 * Setzt die {@link ProjectAUI}.
	 *
	 * @param projectAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setProjectAUI(ProjectAUI projectAUI) {
		this.projectAUI = projectAUI;
	}
		
	
	

}