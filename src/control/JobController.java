package control;

import java.time.LocalDate;
import java.util.ArrayList;

import abstractuserinterfaces.ContextlistAUI;
import abstractuserinterfaces.JobAUI;
import abstractuserinterfaces.ProjectAUI;
import exceptions.EmptyStringParameterException;
import exceptions.GtdJobException;
import exceptions.ObjectNotInGtdException;
import model.Contextlist;
import model.Delegation;
import model.Job;
import model.Project;
import model.Status;
import model.TreeNode;

/**
 * Controller für Funktionen zur Verwaltung der Tätigkeiten.
 *
 * @author Florian
 */
public class JobController {
	/**
	 * Die Referenz auf den zentralen Controller, der zum Austausch zwischen den
	 * Controllern dient.
	 */
	private GtdController gtdController;
	/**
	 * Referenz zum {@link JobAUI}.
	 */
	private JobAUI jobAUI;
	
	/**
	 * Referenz zum {@link ContextlistAUI}.
	 */
	private ContextlistAUI contextlistAUI;

	/**
	 * Referenz zum {@link ProjectAUI}.
	 */
	private ProjectAUI projectTreeAUI;
	
	/**
	 * Konstruktor.
	 *
	 * @param gtdController
	 *            Die Referenz auf den zentralen Controller, der zum Austausch
	 *            zwischen den Controllern dient.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der Parameter
	 *             <em>null</em> ist.
	 * @postconditions Das Attribut gtdController verweist auf den zentralen
	 *                 Controller, sofern nicht <em>null</em> übergeben wurde.
	 */
	public JobController(GtdController gtdController) throws NullPointerException {
		
		ParamVal.objNotNull(gtdController, "gtdController");
		this.gtdController = gtdController;
		
	}

	

	/**
	 * Erzeugt in der Kontextliste mit dem übergebenen Titel eine neue Tätigkeit
	 * mit dem übergebenen Bezeichner.
	 *
	 * @param contextlist
	 *            Die Kontextliste.
	 * @param title
	 *            Der Titel der Tätigkeit.
	 * @param completeUntil
	 *            Das Datum, bis wann die Tätigkeit erledigt sein soll. Muss in
	 *            der Zukunft liegen.
	 * @param description
	 *            Die detaillierte Beschreibung der Tätigkeit. Darf ein leerer
	 *            String, aber nicht <em>null</em> sein.
	 * @return Die erstellte Tätigkeit.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen Parameter <em>null</em> ist.
	 * @throws EmptyStringParameterException
	 *             Die Runtime-Exception wird geworfen, wenn der Titel leer ist.
	 * @throws IllegalArgumentException
	 *             Die Runtime-Exception wird geworfen, wenn das übergebene
	 *             Datum in der Vergangenheit liegt.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn die übergebene
	 *             Kontextliste nicht in Gtd enthalten ist.
	 * @throws GtdJobException
	 *             Die Exception wird geworfen, wenn bereits eine Tätigkeit mit
	 *             dem Titel in der Kontextliste existiert.
	 * @postconditions Eine Tätigkeit mit dem Bezeichner, der Beschreibung und
	 *                 dem Zieldatum ist in der angegebenen Kontextliste
	 *                 vorhanden.
	 */
	public Job createJob(Contextlist contextlist, String title, LocalDate completeUntil, String description)
			throws NullPointerException, EmptyStringParameterException, IllegalArgumentException,
			ObjectNotInGtdException, GtdJobException {
		
		// Überprüfe Parameter: not null or empty, LocalDate muss in der
		// Zukunft, Beschreibung darf nicht null, aber leer sein.
		ParamVal.objNotNull(contextlist, "contextlist");
		ParamVal.stringNotNullOrEmpty(title, "title");
		ParamVal.dateNotNullAndInFuture(completeUntil, "completeUntil");
		ParamVal.objNotNull(description, "description");

		if (!gtdController.getGtd().getContextlists().contains(contextlist)) {
			// Kontextliste nicht im Gtd
			throw new ObjectNotInGtdException("Die Kontextliste ist nicht in Gtd enthalten.");
		}
		// Überprüfe, ob Tätigkeit mit gleichem Titel existiert.
		if (existJobTitle(contextlist, title)) {
			throw new GtdJobException(
					"Eine Tätigkeit mit Titel '" + title + "' existiert bereits in der Kontextliste.");
		}
		// Erzeuge neue Tätigkeit und füge in Kontextliste ein.
		Job job = new Job(title, completeUntil, description);
		contextlist.addJob(job);
		this.jobAUI.refreshJobs();
		this.contextlistAUI.refreshContextlists();
		return job;
		
	}

	/**
	 * Delegiert eine Tätigkeit an eine andere Person bis zu einem bestimmten
	 * Datum.
	 *
	 * @param job
	 *            Die zu delegierende Tätigkeit.
	 * @param delegation
	 *            Das Delegation-Objekt.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen Parameter <em>null</em> ist.
	 * @throws EmptyStringParameterException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen String-Parameter leer ist.
	 * @throws IllegalArgumentException
	 *             Die Runtime-Exception wird geworfen, wenn das übergebene
	 *             Datum in der Vergangenheit liegt.
	 * @throws GtdJobException
	 *             Eine Exception wird geworfen, wenn die Tätigkeit nicht in
	 *             einer Kontextliste ist.
	 */
	public void delegateJob(Job job, Delegation delegation)
			throws NullPointerException, EmptyStringParameterException, IllegalArgumentException, GtdJobException {
		
		// Überprüfe Parameter: not null or empty, LocalDate in der Zukunft
		ParamVal.objNotNull(job, "job");
		ParamVal.objNotNull(delegation, "delegation");
		ParamVal.stringNotNullOrEmpty(delegation.getToPerson(), "delegation.getToPerson");
		ParamVal.dateNotNullAndInFuture(delegation.getUntil(), "delegation.getUntil");

		if (searchContextlist(job) == null) {
			throw new GtdJobException(
					"Die Tätigkeit kann nicht delegiert werden, da sie in keiner Kontextliste (mehr) ist.");
		}
		job.setDelegation(delegation);
		Project project = searchProject(job);
		if (project != null) {
			project.updateNextJob();
			gtdController.getProjectController().updateFinishedStatusInAllProjects();
			projectTreeAUI.refreshProject();
		}
		this.jobAUI.refreshJob();
		
	}

	/**
	 * Überprüft, ob eine Tätgkeit mit dem Titel bereits existiert.
	 *
	 * @param contextlist
	 *            Die Kontextliste in der gesucht werden soll.
	 * @param title
	 *            Der Titel der Tätigkeit, nach der gesucht werden soll.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen Parameter <em>null</em> ist.
	 * @throws EmptyStringParameterException
	 *             Die Runtime-Exception wird geworfen, wenn der Titel leer ist.
	 * @return true, wenn eine Tätigkeit mit dem Titel exisitert.
	 */
	public boolean existJobTitle(Contextlist contextlist, String title)
			throws NullPointerException, EmptyStringParameterException {
		
		ParamVal.objNotNull(contextlist, "contextlist");
		ParamVal.stringNotNullOrEmpty(title, "title");
		for (Job job : contextlist.getJobs()) {
			if (job.getTitle().equals(title)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Verschiebt eine Tätigkeit von einer Kontextliste in eine Andere. Wenn die
	 * Ziel-Kontextliste gleich der Ursprungs-Kontextliste ist, ändert sich
	 * nichts. Wenn in der Ziel-Kontextliste bereits eine Tätigkeit mit dem
	 * gleichen Titel existiert, wird die Tätigkeit nicht verschoben.
	 *
	 * @param job
	 *            Die Tätigkeit
	 * @param newContextlist
	 *            Die neue Kontextliste
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen Parameter <em>null</em> ist.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn die übergebene
	 *             Kontextliste nicht in Gtd enthalten ist.
	 * @throws GtdJobException
	 *             Eine Exception wird geworfen, wenn die Tätigkeit nicht in
	 *             einer Kontextliste ist oder eine andere Tätigkeit mit dem
	 *             gleichen Titel in der Ziel-Kontexliste bereits existiert.
	 */
	public void moveJobToContextlist(Job job, Contextlist newContextlist)
			throws NullPointerException, ObjectNotInGtdException, GtdJobException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(job, "job");
		ParamVal.objNotNull(newContextlist, "newContextlist");
		if (!gtdController.getGtd().getContextlists().contains(newContextlist)) {
			throw new ObjectNotInGtdException("Die Kontextliste ist nicht in Gtd enthalten.");
		}
		Contextlist oldContextlist = searchContextlist(job);
		if (oldContextlist == null) {
			throw new GtdJobException("Die Tätigkeit ist in keiner Kontextliste.");
		}
		if (oldContextlist.equals(newContextlist)) {
			// nichts ändert sich
		} else {
			// Überprüfe, ob eine Tätigkeit mit gleicher Titel in neuer
			// Kontextliste bereits vorhanden ist.
			if (existJobTitle(newContextlist, job.getTitle())) {
				throw new GtdJobException("Eine Tätigkeit mit Titel '" + job.getTitle()
						+ "' existiert bereits in der neuen Kontextliste.");
			}
			oldContextlist.removeJob(job);
			newContextlist.addJob(job);
			this.jobAUI.refreshJobs();
		}
		
	}

	/**
	 * Verschiebt eine Tätigkeit in ein Projekt. Ist die Tätigkeit bereits einem
	 * Projekt zugeordnet, wird diese aus dem alten Projekt entfernt. Wenn der
	 * Projekt-Parameter nicht <em>null</em> ist, wird die Tätigkeit in das neue
	 * Projekt eingefügt.
	 *
	 * @param job
	 *            Die zu verschiebene Tätigkeit.
	 * @param newProject
	 *            Das (neue) Projekt.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn ein Parameter
	 *             <em>null</em> ist.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn eines der Objekte
	 *             nicht in Gtd enthalten ist.
	 * @throws GtdJobException
	 *             Die Exception wird geworfen, wenn bereits eine andere
	 *             Tätigkeit mit dem gleichen Titel im neuen Projekt existiert.
	 */
	public void moveJobToProject(Job job, Project newProject)
			throws NullPointerException, ObjectNotInGtdException, GtdJobException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(job, "job");
		ParamVal.objNotNull(newProject, "newProject");
		Contextlist contextlist = searchContextlist(job);
		Project oldProject = searchProject(job);
		// Teste ob Tätigkeit in Gtd
		if (contextlist == null && oldProject == null) {
			throw new ObjectNotInGtdException("Die Tätigkeit ist nicht in Gtd enthalten.");
		}
		// Teste ob Projekt in Gtd
		TreeNode parentTreeNode = gtdController.getProjectController().searchParentTreeNode(newProject);
		if (parentTreeNode == null) {
			throw new ObjectNotInGtdException("Das Projekt ist nicht in Gtd enthalten.");
		}
		if (newProject.equals(oldProject)) {
			// keine Änderung
			return;
		}
		// Teste, ob andere Tätigkeit im Projekt den gleichen Titel hat.
		for (Job element : newProject.getJobs()) {
			if (element.getTitle().equals(job.getTitle())) {
				throw new GtdJobException(
						"Eine Tätigkeit mit Titel '" + job.getTitle() + "' existiert bereits im Projekt.");
			}
		}
		if (oldProject != null) {
			// Altes Projekt gefunden, entferne Tätigkeit aus altem Projekt.
			oldProject.removeJob(job);
			oldProject.updateNextJob();
		}
		// Füge Tätigkeit zu neuem Projekt hinzu.
		newProject.addJob(job);
		newProject.updateNextJob();
		gtdController.getProjectController().updateFinishedStatusInAllProjects();
		this.jobAUI.refreshJobs();
		
	}

	/**
	 * Entfernt eine Tätigkeite aus dem Projekt(baum) und aktualisiert
	 * anschließend die nächste Tätigkeit und den Projektstatus.
	 *
	 * @param job
	 *            Die zu entfernende Tätigkeit.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             Job-Parameter <em>null</em> ist.
	 * @throws ObjectNotInGtdException
	 *             Die Runtime-Exception wird geworfen, wenn die Tätigkeit nicht
	 *             in Gtd enthalten ist.
	 * @throws GtdJobException
	 *             Die Exception wird geworfen, wenn die Tätigkeit nicht in
	 *             einer Projekt ist.
	 */
	public void removeJobFromProject(Job job) throws NullPointerException, ObjectNotInGtdException, GtdJobException {
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(job, "job");
		Project project = searchProject(job);
		// Teste ob Tätigkeit in Gtd
		if (project == null) {
			throw new GtdJobException("Die Tätigkeit ist nicht in einem Projekt.");
		}
		// Entferne Tätigkeit aus Projekt
		project.removeJob(job);
		project.updateNextJob();
		gtdController.getProjectController().updateFinishedStatusInAllProjects();
		this.jobAUI.refreshJobs();
		this.contextlistAUI.refreshContextlists();
		
	}

	/**
	 * Sucht die Kontextliste, in der die Tätigkeit ist.
	 *
	 * @param job
	 *            Die Tätigkeit für die Suche.
	 * @return Die gefundene Kontextliste mit der Tätigkeit oder <em>null</em>.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der Parameter
	 *             <em>null</em> ist.
	 */
	public Contextlist searchContextlist(Job job) throws NullPointerException {
		ParamVal.objNotNull(job, "job");

		ArrayList<Contextlist> contextlists = gtdController.getGtd().getContextlists();
		for (Contextlist contextlist : contextlists) {
			if (contextlist.getJobs().contains(job)) {
				return contextlist;
			}
		}
		
		return null;
	}

	/**
	 * Sucht mögliches Projekt, zu dem die Tätigkeit gehört.
	 *
	 * @param job
	 *            Die Tätigkeit für die Suche.
	 * @return Das gefundene Projekt mit der Tätigkeit oder <em>null</em>.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn der Parameter
	 *             <em>null</em> ist.
	 */
	public Project searchProject(Job job) throws NullPointerException {
		ParamVal.objNotNull(job, "job");
		return searchProjectRek(job, gtdController.getGtd().getRootNode());
	}

	/**
	 * Rekursive Submethode von searchProject, die den Baum rekursiv nach dem
	 * Projekt mit der Tätigkeit durchsucht.
	 *
	 * @param job
	 *            Die Tätigkeit für die Suche.
	 * @param treeNode
	 *            Der Vaterknoten von Subprojekten.
	 * @return Das gefundene Projekt mit der Tätigkeit oder <em>null</em>.
	 */
	private Project searchProjectRek(Job job, TreeNode treeNode) {
		Project result = null;
		for (int i = 0; result == null && i < treeNode.getSubprojects().size(); i++) {
			Project project = treeNode.getSubprojects().get(i);
			if (project.getJobs().contains(job)) {
				result = project;
			} else {
				result = searchProjectRek(job, project);
			}
		}
		return result;
	}

	/**
	 * Ersetzt den Status einer Tätigkeit mit einem neuen Status-Wert (bis auf
	 * {@link Status#DELEGATED}). Liefert "true", wenn der Status geändert
	 * wurde.
	 *
	 * @param job
	 *            Die Tätigkeit.
	 * @param status
	 *            Der neue Status.
	 * @return Ob der Status geändert wurde.
	 * @throws NullPointerException
	 *             Die Runtime-Exception wird geworfen, wenn einer der
	 *             übergebenen Parameter <em>null</em> ist.
	 * @throws IllegalArgumentException
	 *             Die Runtime-Exception wird geworfen, wenn der übergebene
	 *             Status DELEGATED ist, da hier die Delegationsinformationen
	 *             fehlen. Verwenden Sie stattdessen
	 *             {@link JobController#delegateJob(Job, Delegation)}
	 * @throws GtdJobException
	 *             Die Exception wird geworfen, wenn der Status nicht geändert
	 *             werden kann, da die Tätigkeit in keiner Kontextliste mehr
	 *             ist.
	 */
	public boolean setJobStatus(Job job, Status status) throws NullPointerException, GtdJobException {
		boolean statusWurdeGesetzt = false;
		
		// Überprüfe Parameter: not null
		ParamVal.objNotNull(job, "job");
		// Status darf nicht null oder Status.DELEGIERT sein.
		ParamVal.objNotNull(status, "status");
		if (status == Status.DELEGATED) {
			throw new IllegalArgumentException(
					"Status kann nicht auf DELEGATED gesetzt werden, dazu fehlen die Delegationsinformationen. Verwenden Sie die Methode zum delegieren.");
		}
		if (searchContextlist(job) == null) {
			throw new GtdJobException(
					"Der Status kann nicht geändert werden, da die Tätigkeit in keiner Kontextliste (mehr) ist.");
		}
		statusWurdeGesetzt = job.setStatus(status);
		Project project = searchProject(job);
		if (project != null && statusWurdeGesetzt) {
			project.updateNextJob();
			gtdController.getProjectController().updateFinishedStatusInAllProjects();
			projectTreeAUI.refreshProject();
		}
		this.jobAUI.refreshJobs();
		this.contextlistAUI.refreshContextlist();
		
		
		return statusWurdeGesetzt;
	}
	
	
	/**
	 * Setze das {@link JobAUI}.
	 *
	 * @param jobsAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setJobsAUI(JobAUI jobsAUI) {
		this.jobAUI = jobsAUI;
	}
	
	
	/**
	 * Setze das {@link ProjectAUI}.
	 *
	 * @param projectTreeAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setProjectAUI(ProjectAUI projectTreeAUI) {
		this.projectTreeAUI = projectTreeAUI;
	}
	
	/**
	 * Setze das {@link ContextlistAUI}.
	 *
	 * @param contextlistAUI
	 *            Das neue AbstractUserInterface.
	 */
	public void setContextlistAUI(ContextlistAUI contextlistAUI) {
		this.contextlistAUI = contextlistAUI;
	}
	


}