/**
 * Dieses Package enthält alle Klassen der Control-Schicht.
 * Die Klassen nehmen Nutzereingaben der GUI entgegehn und manipulieren 
 * entsprechend die im Model gespreicherten Daten.
 */
package control;