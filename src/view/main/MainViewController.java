package view.main;

import java.io.IOException;

import control.GtdController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import view.alerts.MainViewAlerts;
import view.contextlist.ContextlistViewController;
import view.inbox.InboxViewController;
import view.job.JobViewController;
import view.project.ProjectViewController;

public class MainViewController extends BorderPane{


	
    
	@FXML
    private BorderPane mainview;

    @FXML
    private Button buttonShowInboxView;

    @FXML
    private Button buttonShowJobView;

    @FXML
    private Button buttonShowContextlistView;

    @FXML
    private Button buttonShowProjectView;

    @FXML
    private Button buttonExitProgram;
	
	
	private InboxViewController inboxViewController;
	
	private ProjectViewController projectViewController;
	
	private ContextlistViewController contextlistViewController;
	
	private JobViewController jobViewController;
	
    private GtdController gtdController;
    
    public MainViewController() {
    	gtdController = new GtdController();
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("MainView.fxml"));
    	loader.setRoot(this);
    	loader.setController(this);
    	try {
			loader.load();
		} catch (IOException e) {
		
		}
    	
    	
    	
    }
    
    

    @FXML
    public void initialize() {
    	inboxViewController = new InboxViewController(gtdController,this);
    	projectViewController = new ProjectViewController(gtdController);
    	contextlistViewController = new ContextlistViewController(gtdController);
    	jobViewController = new JobViewController(gtdController);
    	
    	this.setCenter(inboxViewController);
    }
    
    
   

    @FXML
    void showContextlistView(ActionEvent event) {
    	this.setCenter(contextlistViewController);
    }

    @FXML
    void showInboxView(ActionEvent event) {
    	this.setCenter(inboxViewController);
    }

    @FXML
    void showJobView(ActionEvent event) {
    	this.setCenter(jobViewController);
    }

    @FXML
    void showProjectView(ActionEvent event) {
    	this.setCenter(projectViewController);
    }
    
    @FXML
    void exitProgram(ActionEvent event) {
    	if(MainViewAlerts.exitProgram())
    		Platform.exit();
    }



	public ProjectViewController getProjectViewController() {
		this.setCenter(projectViewController);
		return projectViewController;
	}

	public JobViewController getJobViewController() {
		this.setCenter(jobViewController);
		return jobViewController;
	}
  
    
}
