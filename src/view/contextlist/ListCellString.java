package view.contextlist;

import javafx.scene.control.ListCell;
import model.Contextlist;

public class ListCellString extends ListCell<Contextlist> {
	
	@Override
	protected void updateItem(Contextlist item, boolean empty) {
		super.updateItem(item, empty);
		if(empty)
			this.setText("");
		else
			this.setText(item.getTitle());
	}

}
