package view.contextlist;

import java.io.IOException;
import java.time.LocalDate;

import abstractuserinterfaces.ContextlistAUI;
import control.ContextlistController;
import control.GtdController;
import exceptions.EmptyStringParameterException;
import exceptions.GtdContextlistException;
import exceptions.ObjectNotInGtdException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.Contextlist;
import model.Job;
import model.Status;
import view.alerts.ContextlistAlerts;
import view.job.TableCellLocalDate;

public class ContextlistViewController extends GridPane implements ContextlistAUI{


	@FXML
	private ButtonBar buttonBarEdit;

	@FXML
	private TextField textFieldTitle;

	@FXML
	private TableView<Job> tableJobs;
	
	@FXML
    private TableColumn<Job, String> tableJobsTitle;

    @FXML
    private TableColumn<Job, Status> tableJobsStatus;

    @FXML
    private TableColumn<Job, LocalDate> tableJobsCompleteUntil;

	@FXML
	private ListView<Contextlist> listViewContextlists;
	
	@FXML
	private Button buttonDelete;

	@FXML
	private Button buttonAdd;
	
	@FXML
	private Button buttonRemoveAllFinishedJobs;

    
    private GtdController gtdController;
    
    private ContextlistController contextlistController;
    
   
    
    public ContextlistViewController(GtdController gtdController) {
    	
    	this.gtdController = gtdController;
    	this.contextlistController = this.gtdController.getContextlistController();
    
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("ContextlistView.fxml"));
    	loader.setRoot(this);
    	loader.setController(this);
    	try {
			loader.load();
		} catch (IOException e) {
			
		}
    	
    	
    	
    }

    @FXML
    public void initialize() {

    	
    	this.setEditable(false);
    	this.contextlistController.setContextlistsAUI(this);
    	
    	gtdController.getJobController().setContextlistAUI(this);
    	this.listViewContextlists.setCellFactory(contextlist -> new ListCellString());
    	
    	tableJobsTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
    	tableJobsCompleteUntil.setCellValueFactory(new PropertyValueFactory<>("completeUntil"));
    	tableJobsCompleteUntil.setCellFactory(row -> new TableCellLocalDate());
    	
    	
    	
    	tableJobsStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
    	
    	
    	
    }
    
    
    @FXML
    void addContextlist(ActionEvent event) {
    	setDetails(null);
    	this.setEditable(true);
    	this.textFieldTitle.requestFocus();
    }

    

    @FXML
    void deleteContextlist(ActionEvent event) {
    	
    	
    	try {
    		Contextlist contextlist = this.listViewContextlists.getSelectionModel().getSelectedItem();
			this.contextlistController.removeContextlist(contextlist);
			setDetails(null);
    		
		} catch ( GtdContextlistException e) {
			ContextlistAlerts.deleteContextlistFailed();;
		}catch(NullPointerException | ObjectNotInGtdException e) {
			ContextlistAlerts.noContextlistSelected();
		}
    }



    @FXML
    void selectContextlist(MouseEvent event) {
    	Contextlist contextlist = this.listViewContextlists.getSelectionModel().getSelectedItem();
    	this.setDetails(contextlist);
    }

    
    
    @FXML
    void save(ActionEvent event) {
    	
    	try {
			contextlistController.createContextlist(textFieldTitle.getText());
			this.setEditable(false);
			setDetails(null);
		} catch (NullPointerException | EmptyStringParameterException | GtdContextlistException e) {
			
			ContextlistAlerts.noContextlistNameAlert();
		}
    	
    }
    
    @FXML
    void cancel(ActionEvent event) {
    	this.setEditable(false);
		setDetails(null);	
    	
    }


    

    @FXML
    void removeAllFinishedJobs(ActionEvent event) {
    	
    	
		this.contextlistController.removeFinishedJobsInAllContextlists();
	
    }

	
	@Override
	public void refreshContextlist() {
		Contextlist contextlist = listViewContextlists.getSelectionModel().getSelectedItem();
		
		setDetails(contextlist);
		
	}
	
	@Override
	public void refreshContextlists() {
	
		refreshContextlist();
		this.listViewContextlists.setItems(FXCollections.observableList(this.gtdController.getGtd().getContextlists()));
		
		
	}
	
	
	
	
	private void setEditable(boolean editable) {
		this.buttonBarEdit.setVisible(editable);
		this.textFieldTitle.setEditable(editable);
		this.buttonAdd.setVisible(!editable);
		this.buttonDelete.setVisible(!editable);
		this.buttonRemoveAllFinishedJobs.setVisible(!editable);
		
		
	}
	private void setDetails(Contextlist contextlist) {
		if(contextlist == null) {
			this.textFieldTitle.setText("");
			this.tableJobs.setItems(FXCollections.emptyObservableList());
			this.tableJobs.refresh();
		}
		else {
			this.textFieldTitle.setText(contextlist.getTitle());
			this.tableJobs.setItems(FXCollections.observableList(contextlist.getJobs()));
			this.tableJobs.refresh();
		}
	}

	
    


    
}
