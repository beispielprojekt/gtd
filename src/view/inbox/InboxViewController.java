package view.inbox;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import abstractuserinterfaces.InboxAUI;
import control.GtdController;
import control.InboxController;
import exceptions.EmptyStringParameterException;
import exceptions.ObjectNotInGtdException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.Note;
import view.alerts.InboxAlerts;
import view.main.MainViewController;

public class InboxViewController extends GridPane implements InboxAUI{


    @FXML
    private TableView<Note> tableInbox;
	
	@FXML
	private TableColumn<Note, LocalDateTime> tableInboxCreationDate;

	@FXML
    private TableColumn<Note, String> tableInboxText;

    @FXML
    private DatePicker datePickerCreationDate;

    @FXML
    private TextArea textAreaText;

    @FXML
    private Button buttonEdit;

    @FXML
    private ButtonBar buttonBarEdit;
    
    @FXML
    private Button buttonRemove;

    @FXML
    private Button buttonChangeToProject;

    @FXML
    private Button buttonChangeToJob;

    @FXML
    private Button buttonAdd;
    
    private GtdController gtdController;
    
    private InboxController inboxController;
    
    private MainViewController mainViewController;
    
    public InboxViewController(GtdController gtdController, MainViewController mainViewController) {
    	
    	this.gtdController = gtdController;
    	this.inboxController = gtdController.getInboxController();
    	this.mainViewController = mainViewController;
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("InboxView.fxml"));
    	loader.setRoot(this);
    	loader.setController(this);
    	try {
			loader.load();
		} catch (IOException e) {
	
		}
    	
    	
    	
    }

    @FXML
    public void initialize() {
    	gtdController.getInboxController().setInboxAUI(this);
    	
    	tableInboxText.setCellValueFactory(new PropertyValueFactory<>("text"));
    	tableInboxCreationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
    	tableInboxCreationDate.setCellFactory(row -> new TableCellLocalDateTime());
    	this.setEditable(false);
    	this.setDetails(null);
    	 
    	refreshInbox();
    }
    
    
    @FXML
    void addNote(ActionEvent event) {
    	this.setEditable(true);
    	this.setDetails(null);
    	
    	
    	
    }

	@FXML
    void changeToJob(ActionEvent event) {
		Note note = tableInbox.getSelectionModel().getSelectedItem();
		if(note != null) {
			mainViewController.getJobViewController().createJobFromNote(note);
		}
		else 	
			InboxAlerts.noNoteChoosed();
			
    }

    @FXML
    void changeToProject(ActionEvent event) {
    	Note note = tableInbox.getSelectionModel().getSelectedItem();
		if(note != null) {
			mainViewController.getProjectViewController().createProjectFromNote(note);
		}
		else
			InboxAlerts.noNoteChoosed();
    }

    @FXML
    void deleteNote(ActionEvent event) {
    	
    	
    	try {
    		Note selectedNote = tableInbox.getSelectionModel().getSelectedItem();
			inboxController.removeNote(selectedNote);
    	}
    	catch(NullPointerException| ObjectNotInGtdException exception) {
    		InboxAlerts.noNoteChoosed();
    	}
    }
    
    
    @FXML
    void save(ActionEvent event) {
    	
    	String inputText = textAreaText.getText();
    	
    	try {
    		inboxController.createNote(inputText);
    		setDetails(null);
    		this.setEditable(false);
    	}
    	catch(NullPointerException | EmptyStringParameterException exception) {
    		InboxAlerts.noNoteText();
    		
    	}
    
    }
    
    @FXML
    void cancel(ActionEvent event) {
    	this.setEditable(false);
    	setDetails(null);
    }

    @FXML
    void selectNote(MouseEvent event) {
    	Note selectedNote = this.tableInbox.getSelectionModel().getSelectedItem();
    	
    	setDetails(selectedNote);
    	
    }
    

    
    //Implementierung der InboxAUI-Methoden
    
	@Override
	public void refreshInbox() {
		List<Note> inboxList = gtdController.getGtd().getInbox().getList();
		tableInbox.setItems(FXCollections.observableArrayList(inboxList));
		tableInbox.refresh();		
		
	}

	
	
	private void setEditable(boolean editable) {
		this.buttonBarEdit.setVisible(editable);
		this.datePickerCreationDate.setDisable(true);
		datePickerCreationDate.setStyle("-fx-opacity: 1");
		datePickerCreationDate.getEditor().setStyle("-fx-opacity: 1");
		
		this.textAreaText.setEditable(editable);
		if(editable)
			this.textAreaText.requestFocus();
	
		this.buttonAdd.setVisible(!editable);
		this.buttonRemove.setVisible(!editable);
		this.buttonChangeToJob.setVisible(!editable);
		this.buttonChangeToProject.setVisible(!editable);
	
		
	}
	private void setDetails(Note note) {
		if(note == null) {
			this.datePickerCreationDate.setValue(null);
			this.datePickerCreationDate.setPromptText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
			this.textAreaText.setText("");
	
		}
		else {
			this.datePickerCreationDate.setValue(note.getCreationDate().toLocalDate());
			this.textAreaText.setText(note.getText());
			this.datePickerCreationDate.setValue(LocalDate.now());
		}
	}
    


    
}
