package view.inbox;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javafx.scene.control.TableCell;
import model.Note;

public class TableCellLocalDateTime extends TableCell<Note, LocalDateTime>{

	
	private static DateTimeFormatter dtf =  DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	
	
	@Override
	protected void updateItem(LocalDateTime item, boolean empty) {
		if(!empty) {
			String value = dtf.format(item);
			setText(value);
		}
			
	}
}
