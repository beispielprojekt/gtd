/**
 * Dieses Package enthält alle GUI-Komponenten des Beispielprojektes GTD.
 * Dazu gehöhren die JavaFX-Dateien und die ViewController-Klassen.
 */

package view;