package view.project;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import abstractuserinterfaces.ProjectAUI;
import control.GtdController;
import control.ProjectController;
import exceptions.EmptyStringParameterException;
import exceptions.GtdProjectException;
import exceptions.ObjectNotInGtdException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.Job;
import model.Note;
import model.Project;
import model.TreeNode;
import view.alerts.ProjectAlerts;
import view.job.TableCellLocalDate;

public class ProjectViewController extends GridPane implements ProjectAUI{



    @FXML
    private TreeView<TreeNode> treeViewProjects;

    @FXML
    private TextField textFieldTitle;

    @FXML
    private TextArea textAreaDiscription;

    @FXML
    private TableView<Job> tableJobs;

    @FXML
    private TableColumn<Job, LocalDate> tableJobsCompleteUntil;

    @FXML
    private TableColumn<Job, String> tableJobsTitle;

    @FXML
    private TableColumn<Job, Boolean> tableJobsStatus;

    @FXML
    private ChoiceBox<TreeNode> choiseBoxParent;
    

    @FXML
    private ChoiceBox<TreeNode> choiseBoxMoveParent;

    @FXML
    private DatePicker datePickerCompleteUntil;

    @FXML
    private ProgressBar progressBarStatus;

    
    @FXML
    private ButtonBar buttonBarEdit;
    
    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonDelete;

    @FXML
    private Button buttonMove;
   

    
    private GtdController gtdController;
    
    private ProjectController projectController;
    
    
    public ProjectViewController(GtdController gtdController) {
    	
    	this.gtdController = gtdController;
    	
    	this.projectController = gtdController.getProjectController();
    
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("ProjectView.fxml"));
    	loader.setRoot(this);
    	loader.setController(this);
    	try {
			loader.load();
		} catch (IOException e) {
			
		}
    	
    	
    	
    }
    
    
    @FXML
    public void initialize() {

    	
    	this.setEditable(false);
    	this.projectController.setProjectTreeAUI(this);
    	this.gtdController.getJobController().setProjectAUI(this);
    	this.gtdController.getContextlistController().setProjectAUI(this);
    	this.treeViewProjects.setCellFactory(cell -> new TreeCellTitle());
    	this.choiseBoxParent.setConverter(new ChoiceBoxConverterTreeNode());
    	this.choiseBoxMoveParent.setConverter(new ChoiceBoxConverterTreeNode());
    	this.setDetails(null);
    	
    	
    	tableJobsTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
    	tableJobsCompleteUntil.setCellValueFactory(new PropertyValueFactory<>("completeUntil"));
    	tableJobsCompleteUntil.setCellFactory(row -> new TableCellLocalDate());
    	
    	tableJobsStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
    	
    	
    	refreshProjectTree();
    	
    	this.datePickerCompleteUntil.setStyle("-fx-opacity: 1");
    	this.datePickerCompleteUntil.getEditor().setStyle("-fx-opacity: 1");
    	this.choiseBoxMoveParent.setStyle("-fx-opacity: 1");
    	this.choiseBoxParent.setStyle("-fx-opacity: 1");
    	
    }
    
    @FXML
    void addProject(ActionEvent event) {
    	this.setEditable(true);
    	this.setDetails(null);
    	this.datePickerCompleteUntil.setValue(LocalDate.now().plusDays(1));
    	TreeNode rootNode = gtdController.getGtd().getRootNode();
    	this.choiseBoxParent.setValue(rootNode);
    	
    }

    @FXML
    void moveProjectTo(ActionEvent event) {
    	
    	
    	
   		try {
    		TreeItem<TreeNode> selectedItem = this.treeViewProjects.getSelectionModel().getSelectedItem();
    		if(selectedItem != null && selectedItem.getValue() instanceof Project) {
   	    	TreeNode project = selectedItem.getValue();
        	TreeNode newParent = choiseBoxMoveParent.getValue();
       		this.setDetails(null);
       		this.projectController.moveProject((Project)project, newParent);
   			}
			
		} catch (NullPointerException | ObjectNotInGtdException | GtdProjectException e) {				
			ProjectAlerts.moveProjectNotPossible();
		}
    	
    	
    }

    @FXML
    void removeProject(ActionEvent event) {
    	TreeItem<TreeNode> selectedTreeItem = this.treeViewProjects.getSelectionModel().getSelectedItem();
    	if(selectedTreeItem != null && selectedTreeItem.getValue() instanceof Project) {
    		Project selectedProject = (Project)selectedTreeItem.getValue(); 
    		try {
				this.projectController.removeProject(selectedProject);
				this.setDetails(null);
			} catch (NullPointerException | ObjectNotInGtdException | GtdProjectException e) {
				ProjectAlerts.noProjectSelected();
			}
    	}
    }

 

   
    @FXML
    void save(ActionEvent event) {
    	
    	try {
    		
        	TreeNode parent = choiseBoxParent.getValue();
        	String title = textFieldTitle.getText();
        	LocalDate completeUntil = datePickerCompleteUntil.getValue();
        	String description = textAreaDiscription.getText();
			this.projectController.createProject(parent, title, completeUntil, description);
			setDetails(null);
			this.setEditable(false);
		} catch (NullPointerException | EmptyStringParameterException | IllegalArgumentException
				| ObjectNotInGtdException | GtdProjectException e) {
			ProjectAlerts.noProjectNameAlert();
		}
    	
    }
    
    @FXML
    void cancel(ActionEvent event) {
    	this.setEditable(false);
    	setDetails(null);
    }
    
    
    @FXML
    void selectProject(MouseEvent event) {
    	TreeItem<TreeNode> selectedItem = treeViewProjects.getSelectionModel().getSelectedItem();
    	if(selectedItem != null) {
    		TreeNode selectedProject = selectedItem.getValue();
    		if(selectedProject instanceof Project)
        		setDetails((Project)selectedProject);
    		
    		List<TreeNode> allProjects = this.projectController.allTreeNodes();
    		this.choiseBoxMoveParent.setItems(FXCollections.observableList(allProjects));
    		
    	}
    	
    }


   //Implementierung der ProjectTreeAUI-Methoden

    
	@Override
	public void refreshProject() {
		TreeItem<TreeNode> selectedTreeNode = treeViewProjects.getSelectionModel().getSelectedItem();
		if(selectedTreeNode != null && selectedTreeNode.getValue() instanceof Project) {
			setDetails((Project)selectedTreeNode.getValue());
		}
		else {
			setDetails(null);
		}
	}

	@Override
	public void refreshProjectTree() {
		
		
		refreshProject();
		
		TreeNode treeNode = gtdController.getGtd().getRootNode();
		TreeItem<TreeNode> root = new TreeItem<TreeNode>(treeNode);
		treeViewProjects.setRoot(addProjects(treeNode, root));
		treeViewProjects.refresh();
		List<TreeNode> allProjects = this.projectController.allTreeNodes();
		this.choiseBoxMoveParent.setItems(FXCollections.observableList(allProjects));
		this.choiseBoxMoveParent.setValue(gtdController.getGtd().getRootNode());
		
		
	}
	
	public void createProjectFromNote(Note note) {
		setEditable(true);
		this.textFieldTitle.setText(note.getText());
	}
	
	private TreeItem<TreeNode> addProjects(TreeNode treeNode, TreeItem<TreeNode> root){
		root.setValue(treeNode);
		for(Project project:treeNode.getSubprojects()) {
			root.getChildren().add(addProjects(project, new TreeItem<TreeNode>(treeNode)));
		}
		
		return root;
	}
	
	
	private void setEditable(boolean editable) {
		this.buttonBarEdit.setVisible(editable);
		this.textAreaDiscription.setEditable(editable);
		this.datePickerCompleteUntil.setEditable(editable);
		this.datePickerCompleteUntil.setDisable(!editable);
		this.textFieldTitle.setEditable(editable);
		this.choiseBoxParent.setDisable(!editable);
		
		this.buttonAdd.setVisible(!editable);
		this.buttonDelete.setVisible(!editable);
		this.buttonMove.setVisible(!editable);
		this.choiseBoxMoveParent.setVisible(!editable);
		
		this.textFieldTitle.requestFocus();
		
		
	}
    
	private void setDetails(Project project) {
		if(project == null) {
			this.textFieldTitle.setText("");
			List<TreeNode> allProjects = this.projectController.allTreeNodes();
			this.choiseBoxParent.setItems(FXCollections.observableList(allProjects));
			this.choiseBoxParent.setValue(null);
			this.textAreaDiscription.setText("");
			this.datePickerCompleteUntil.setValue(null);
			this.tableJobs.setItems(FXCollections.emptyObservableList());
			this.tableJobs.refresh();
			this.progressBarStatus.setProgress(0);
			this.datePickerCompleteUntil.setPromptText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		}			
		else {
			this.textFieldTitle.setText(project.getTitle());
			List<TreeNode> allProjects = this.projectController.allTreeNodes();
			this.choiseBoxParent.setItems(FXCollections.observableList(allProjects));
			TreeNode parent = this.gtdController.getProjectController().searchParentTreeNode(project);
			this.choiseBoxParent.setValue(parent);
			this.datePickerCompleteUntil.setValue(project.getCompleteUntil());
			this.tableJobs.setItems(FXCollections.observableList(project.getJobs()));
			this.textAreaDiscription.setText(project.getDescription());
			this.tableJobs.refresh();
			double progress = this.projectController.calculateProjectProgress(project);
			this.progressBarStatus.setProgress(progress);
		}
	}
	
	
   
    
    
}
