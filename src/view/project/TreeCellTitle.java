package view.project;

import javafx.scene.control.TreeCell;
import model.TreeNode;

public class TreeCellTitle extends TreeCell<TreeNode>{

	@Override
	protected void updateItem(TreeNode item, boolean empty) {
		// TODO Auto-generated method stub
		super.updateItem(item, empty);
		if(empty) {
			this.setText("");
		}
		else {
			this.setText(item.getTitle());
		}
		
	}
}
