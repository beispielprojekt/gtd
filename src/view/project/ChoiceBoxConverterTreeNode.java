package view.project;

import javafx.util.StringConverter;
import model.TreeNode;

public class ChoiceBoxConverterTreeNode extends StringConverter<TreeNode>{

	@Override
	public TreeNode fromString(String string) {
		
		return null;
	}

	@Override
	public String toString(TreeNode object) {
		
		return object.getTitle();
	}

}
