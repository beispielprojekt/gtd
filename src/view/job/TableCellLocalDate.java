package view.job;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javafx.scene.control.TableCell;
import model.Job;

public class TableCellLocalDate extends TableCell<Job, LocalDate>{

	
	private static DateTimeFormatter dtf =  DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	
	
	@Override
	protected void updateItem(LocalDate item, boolean empty) {
		if(!empty) {
			String value = dtf.format(item);
			setText(value);
		}
			
	}
}
