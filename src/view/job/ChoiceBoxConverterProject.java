package view.job;

import javafx.util.StringConverter;
import model.Project;

public class ChoiceBoxConverterProject extends StringConverter<Project>{

	@Override
	public Project fromString(String string) {
		
		return null;
	}

	@Override
	public String toString(Project object) {
		
		return object.getTitle();
	}

}
