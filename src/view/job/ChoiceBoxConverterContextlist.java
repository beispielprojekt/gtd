package view.job;

import javafx.util.StringConverter;
import model.Contextlist;

public class ChoiceBoxConverterContextlist extends StringConverter<Contextlist>{

	@Override
	public Contextlist fromString(String string) {
		
		return null;
	}

	@Override
	public String toString(Contextlist object) {
		
		return object.getTitle();
	}

}
