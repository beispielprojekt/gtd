package view.job;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import abstractuserinterfaces.JobAUI;
import control.GtdController;
import control.JobController;
import exceptions.EmptyStringParameterException;
import exceptions.GtdJobException;
import exceptions.ObjectNotInGtdException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.Contextlist;
import model.Delegation;
import model.Job;
import model.Note;
import model.Project;
import model.Status;
import model.TreeNode;
import view.alerts.JobAlerts;

public class JobViewController extends GridPane implements JobAUI{


	 @FXML
	 private ButtonBar buttonBarEdit;

    @FXML
    private TextField textFieldTitle;
	
    @FXML
    private TableView<Job> tableJobs;

    @FXML
    private TableColumn<Job, String> tableJobsTitle;

    @FXML
    private TableColumn<Job, LocalDate> tableJobsCompleteUntil;

    @FXML
    private TableColumn<Job, Status> tableJobsStatus;

    @FXML
    private DatePicker datePickerCompleteUntil;

    @FXML
    private ChoiceBox<Contextlist> choiceBoxContextlist;

    @FXML
    private TextArea textAreaDiscription;

    @FXML
    private ChoiceBox<Project> choiceBoxProject;

    @FXML
    private RadioButton radioButtonEditing;

    @FXML
    private RadioButton radioButtonSometime;

    @FXML
    private RadioButton radioButtonDelegated;

    @FXML
    private TextField textFieldDelegatedToPerson;

    @FXML
    private DatePicker datePickerDelegatedCompleteUntil;
	
    @FXML
    private GridPane gridPaneDelegation;
    
    @FXML
    private Button buttonDelete;

    @FXML
    private Button buttonAdd;
    
    @FXML
    private Button buttonFinishJob;
    
    

    
    private GtdController gtdController;
    
    private JobController jobController;
    
   
    
    public JobViewController(GtdController gtdController) {
    	
    	this.gtdController = gtdController;
    	this.jobController = this.gtdController.getJobController();
    	
    	jobController.setJobsAUI(this);
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("JobView.fxml"));
    	loader.setRoot(this);
    	loader.setController(this);
    	try {
			loader.load();
		} catch (IOException e) {}
    	
    	
    	
    }

    @FXML
    public void initialize() {

    	
    	this.setEditable(false);
    	this.jobController.setJobsAUI(this);
    	gtdController.getContextlistController().setJobAUI(this);
    	
    	ToggleGroup toggleGroup = new ToggleGroup();
    	radioButtonDelegated.setToggleGroup(toggleGroup);
    	radioButtonEditing.setToggleGroup(toggleGroup);
    	radioButtonSometime.setToggleGroup(toggleGroup);
    
    	
  
    	tableJobsTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
    	tableJobsCompleteUntil.setCellValueFactory(new PropertyValueFactory<>("completeUntil"));
    	tableJobsCompleteUntil.setCellFactory(row -> new TableCellLocalDate());
    	
    	
    	
    	tableJobsStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
    	
    	choiceBoxContextlist.setConverter(new ChoiceBoxConverterContextlist());
    	choiceBoxProject.setConverter(new ChoiceBoxConverterProject());
    	
    	this.datePickerCompleteUntil.setStyle("-fx-opacity: 1");
    	this.datePickerCompleteUntil.getEditor().setStyle("-fx-opacity: 1");
    	
    	this.datePickerDelegatedCompleteUntil.setStyle("-fx-opacity: 1");
    	this.datePickerDelegatedCompleteUntil.getEditor().setStyle("-fx-opacity: 1");
    	
    	this.choiceBoxContextlist.setStyle("-fx-opacity: 1");
    	this.choiceBoxProject.setStyle("-fx-opacity: 1");
    	
    }
    
    
    

    
    


    @FXML
    void addJob(ActionEvent event) {
    	List<Contextlist> contextlists = this.gtdController.getGtd().getContextlists();
    	
    	if(contextlists != null && contextlists.size() > 0) {
    		setDetails(null);
        	this.setEditable(true);
        	this.choiceBoxContextlist.setItems(FXCollections.observableList(contextlists));
        	this.choiceBoxContextlist.setValue(contextlists.get(0));
        	
      
        	List<TreeNode> allTreeNodes = this.gtdController.getProjectController().allTreeNodes();
        	ObservableList<Project> allProjects = FXCollections.observableArrayList();
    		for(TreeNode treeNode:allTreeNodes) {
    			if(treeNode instanceof Project) {
    				allProjects.add((Project)treeNode);
    			}
    		}
    		this.choiceBoxProject.setItems(allProjects);
    		this.datePickerCompleteUntil.setValue(LocalDate.now().plusDays(1));
    	}
    	else {
    		JobAlerts.noContextlists();
    	}
    	
    }


    @FXML
    void deleteJob(ActionEvent event) {
    	Job job = tableJobs.getSelectionModel().getSelectedItem();
    	try {
			jobController.removeJobFromProject(job);
		} catch (NullPointerException | ObjectNotInGtdException | GtdJobException e) {
			JobAlerts.jobNotExists();
		}
    	this.setDetails(null);
    }

    @FXML
    void finishJob(ActionEvent event) {

    	
    	try {
    		Job job = tableJobs.getSelectionModel().getSelectedItem();
			this.jobController.setJobStatus(job, Status.FINISHED);
			setDetails(job);
			
			
		} catch (NullPointerException | GtdJobException e) {
			
			JobAlerts.jobNotExists();
		}
    	
    }


    @FXML
    void save(ActionEvent event) {
    	this.setEditable(false);
    	try {
    		
			Job job = this.jobController.createJob(this.choiceBoxContextlist.getSelectionModel().getSelectedItem(), this.textFieldTitle.getText(), this.datePickerCompleteUntil.getValue(), this.textAreaDiscription.getText());
			
			if(radioButtonDelegated.isSelected()) {
				job.setDelegation(new Delegation(this.textFieldDelegatedToPerson.getText(), this.datePickerDelegatedCompleteUntil.getValue()));
				job.setStatus(Status.DELEGATED);
			}
			else if(radioButtonEditing.isSelected()) {
				job.setStatus(Status.EDITING);
			}
			else if(radioButtonSometime.isSelected()) {
				job.setStatus(Status.SOMETIME);
			}
			
			Project project = this.choiceBoxProject.getValue();
			if(project != null) {
				this.jobController.moveJobToProject(job, project);
			}
			
			
		} catch (NullPointerException | EmptyStringParameterException | IllegalArgumentException  e) {
			JobAlerts.illegalArguments();
			
		}
    	catch(ObjectNotInGtdException onige) {
    		JobAlerts.jobNotExists();
    	}
    	catch(GtdJobException gje) {
    		JobAlerts.jobAlreadyExists();
    	}
    	
    	setDetails(null);
    	
    	
    }
    
    @FXML
    void cancel(ActionEvent event) {
    	this.setEditable(false);
    	
    	setDetails(null);
    }

    
    @FXML
    void selectedJob(MouseEvent event) {
    	Job job = tableJobs.getSelectionModel().getSelectedItem();
    	setDetails(job);
    }
    
    @FXML
    void delegated(ActionEvent event) {
    	gridPaneDelegation.setVisible(true);
    }


    @FXML
    void notDelegated(ActionEvent event) {
    	gridPaneDelegation.setVisible(false);
    }

   
    
	
	
    //Implementierung der AUI-Methoden von JobAUI und ContextlistAUI

	@Override
	public void refreshJob() {
		Job job = tableJobs.getSelectionModel().getSelectedItem();
		this.setDetails(job);
		
	}

	@Override
	public void refreshJobs() {
		List <Job> jobs = new ArrayList<Job>();
		for(Contextlist contextlist : gtdController.getGtd().getContextlists()) {
			jobs.addAll(contextlist.getJobs());
		}
		tableJobs.setItems(FXCollections.observableList(jobs));
		tableJobs.refresh();
	}

	
	
	public void createJobFromNote(Note note) {
		setEditable(true);
		this.textFieldTitle.setText(note.getText());
	}
	
	
	private void setEditable(boolean editable) {
		this.buttonBarEdit.setVisible(editable);
		this.buttonAdd.setDisable(editable);
		this.buttonDelete.setDisable(editable);
		this.buttonFinishJob.setDisable(editable);
		
		this.textFieldTitle.setEditable(editable);
		this.datePickerCompleteUntil.setEditable(editable);
		this.choiceBoxContextlist.setDisable(!editable);
		this.choiceBoxProject.setDisable(!editable);
		
		this.textAreaDiscription.setEditable(editable);
		this.radioButtonDelegated.setDisable(!editable);
		this.radioButtonEditing.setDisable(!editable);
		this.radioButtonSometime.setDisable(!editable);
		
		this.textFieldDelegatedToPerson.setEditable(editable);
		this.datePickerDelegatedCompleteUntil.setEditable(editable);
		
		textFieldTitle.requestFocus();
		
		this.gridPaneDelegation.setVisible(this.radioButtonDelegated.isSelected()&&editable);
		
		this.buttonAdd.setVisible(!editable);
		this.buttonDelete.setVisible(!editable);
		this.buttonFinishJob.setVisible(!editable);
		
		this.datePickerCompleteUntil.setDisable(!editable);
		this.datePickerDelegatedCompleteUntil.setDisable(!editable);
		
	}
	private void setDetails(Job job) {
		if(job == null){
			this.textFieldTitle.setText("");
			this.datePickerCompleteUntil.setValue(null);
			this.choiceBoxContextlist.setValue(null);
			this.choiceBoxProject.setValue(null);
			this.textAreaDiscription.setText("");
			
			this.textFieldDelegatedToPerson.setText("");
			this.datePickerDelegatedCompleteUntil.setValue(null);
			this.radioButtonDelegated.getToggleGroup().selectToggle(null);
			this.datePickerCompleteUntil.setPromptText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
			this.datePickerDelegatedCompleteUntil.setPromptText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		}
		else {
			this.textFieldTitle.setText(job.getTitle());
			this.datePickerCompleteUntil.setValue(job.getCompleteUntil());
			//Projekt und Kontextlist ermitteln
			this.choiceBoxContextlist.setItems(FXCollections.observableList(this.gtdController.getGtd().getContextlists()));
			this.choiceBoxContextlist.setValue(this.jobController.searchContextlist(job));
			List<TreeNode> allTreeNodes = this.gtdController.getProjectController().allTreeNodes();
			
			ObservableList<Project> allProjects = FXCollections.observableArrayList();
			for(TreeNode treeNode:allTreeNodes) {
				if(treeNode instanceof Project) {
					allProjects.add((Project)treeNode);
				}
			}
			this.choiceBoxProject.setItems(allProjects);
			Project project = this.jobController.searchProject(job);
			this.choiceBoxProject.setValue(project);
			
			this.textAreaDiscription.setText(job.getDescription());
			
			this.gridPaneDelegation.setVisible(false);
			switch(job.getStatus()) {
				case DELEGATED: radioButtonDelegated.setSelected(true);
					this.gridPaneDelegation.setVisible(true);
					this.textFieldDelegatedToPerson.setText(job.getDelegation().getToPerson());
					this.datePickerDelegatedCompleteUntil.setValue(job.getDelegation().getUntil());
				
				;break;
				case EDITING: radioButtonEditing.setSelected(true);break;
				case FINISHED: radioButtonDelegated.getToggleGroup().selectToggle(null);break;
				case SOMETIME: radioButtonSometime.setSelected(true);break;
			}
		}
	}

	
    


    
}
