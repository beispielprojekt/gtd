package view.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class InboxAlerts {

	
	
	
	public static void noNoteChoosed() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte eine Tätigkeit auswählen.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	
	public static void noNoteText() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte geben Sie eine Beschreibung der Tätigkeit ein.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	

	

}
