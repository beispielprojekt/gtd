package view.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ProjectAlerts {


	
	public static void noProjectNameAlert() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte geben Sie einen Namen für das Projekt ein.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	
	



	
	public static void noProjectSelected() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte wählen Sie ein Projekt aus.");
		alert.setHeaderText("Information");
		alert.showAndWait();
		
	}
	
	public static void moveProjectNotPossible() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Diese Verschiebung des Projektes ist nicht möglich.");
		alert.setHeaderText("Information");
		alert.showAndWait();
		
	}
	

}
