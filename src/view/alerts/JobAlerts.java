package view.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class JobAlerts {

	public static void noContextlists() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte zuerst eine Kontextliste erstellen.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	
	
	public static void jobAlreadyExists() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Eine Tätigkeit mit diesem Namem existiert bereits.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	
	public static void jobNotExists() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte wählen Sie eine Tätigkeit aus.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	
	
	public static void illegalArguments() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte geben Sie gültige Argumente als Parameter an.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
}
