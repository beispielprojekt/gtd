package view.alerts;



import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class MainViewAlerts {


	

	
	public static boolean exitProgram() {
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Wollen Sie das Programm GTD wirklich beenden?");
		alert.setHeaderText("Information");
		alert.getButtonTypes().clear();
		alert.getButtonTypes().add(ButtonType.YES);
		alert.getButtonTypes().add(ButtonType.NO);
		Optional<ButtonType> choosedButtonType = alert.showAndWait();
		if(choosedButtonType.isPresent() && choosedButtonType.get() == ButtonType.YES)
			return true;
		else 
			return false;
		
	}
}
