package view.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ContextlistAlerts {


	
	public static void noContextlistNameAlert() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte geben Sie einen Namen für die Kontextliste ein.");
		alert.setHeaderText("Information");
		alert.showAndWait();
	}
	
	



	
	public static void noContextlistSelected() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Bitte wählen Sie eine Kontextliste aus.");
		alert.setHeaderText("Information");
		alert.showAndWait();
		
	}
	
	public static void deleteContextlistFailed() {
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setContentText("Die Kontextliste kann nicht gelöscht werden, da Sie noch Tätigkeiten enthält.");
		alert.setHeaderText("Information");
		alert.showAndWait();
		
		
	}
}
